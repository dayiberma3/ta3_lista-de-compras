package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }


    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        // ESTABLECE LA CONEXIÓN CON NUESTRA BASE DE DATOS PARA PODER EDITARLA
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        //Se crean los registros mediante los values, para poder ingresar datos a nuestra tabla
        ContentValues values = new ContentValues();

        // Agregamos mediante el put los values que deseamos en nuestra tabla por mediante referencias
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE,productName);

        //Se aplica el metodo INSERT para ingresar valores a la tabla, seguido del nombre y definimo el campo que nos devuelva
        db.insert(ShoppingElementEntry.TABLE_NAME, null, values);
    }


    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query
            allColumns,                         // The columns to return
            null,                               // The columns for the WHERE clause
            null,                               // The values for the WHERE clause
            null,                               // don't group the rows
            null,                               // don't filter by row groups
            null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos

        // SE ESTABLECE LA CONEXIÓN CON LA ESTRUCTURA DE NUESTRA BASE DE DATOS PARA PODER EDITARLA
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // DEFINIMOS EL METODO DELETE PARA ELIMINAR REGISTROS DE LA TABLA, SEGUIDO DEL NOMBRE DE LA TABLA Y DEFINIMOS LOS VALORES POR DEFECTO NULL
        db.delete(ShoppingElementEntry.TABLE_NAME,null,null);

    }

    public void updateItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos

        // SE ESTABLECE LA CONEXIÓN CON LA ESTRUCTURA DE NUESTRA BASE DE DATOS PARA PODER EDITARLA
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // SE CREAN LOS REGISTROS POR MEDIOS DE VALUES PARA INGREASAR LOS DATOS A LA TABLA
        ContentValues values = new ContentValues();

        //AGREAMOS POR MEDIO DEL PUT LOS VALUES QUE QUERAMOS A NUESTRA TABLA POR MEDIO DE LAS REFERENCIAS
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE,shoppingItem.getName());

        //SE ACTUALIZA LOS VALORES DE LA TABALA MEDIANTE LOS TÍTULOS DE LOS PRODUCTOS
        String selection = ShoppingElementEntry._ID + " = ?";

        // SE DEFINE LA POSICIÓN POR MEDIO DE LOS IDENTIFICADORES DE LOS VALORES A MODIFICAR DENTRO DE LA TABLA
        String[] selectionArgs = {String.valueOf(shoppingItem.getId())};

        // SE USA EL MÉTODO UPDATE PARA MODIFICAR LOS VALORES DE LA TABLA, SEGUIDO DEL NOMBRE DE LA TABLA Y LOS NUEVOS VALOES DEFINIDOS EN EL ARRAY
        int updateRows = db.update(ShoppingElementEntry.TABLE_NAME,values,selection,selectionArgs);

    }

    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos

        // SE ESTABLECE LA CONEXIÓN CON LA ESTRUCTURA DE NUESTRA BASE DE DATOS PARA PODER EDITARLA
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // SE ESTABLECE EL LUGAR EN EL CUAL SE VAN A ELMINAR LOS VALORES
        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE + " LIKE ?";

        // SE DEFINE LOS NOMBRES DE LOS VALORES A MODIFICAR DENTRO DE LA TABLA
        String[] selectionArgs = {shoppingItem.getName()};

        // SE USA EL MÉTODO ELIMINAR, SEGUIDO DEL NOMBRE DE LA TABLA Y EL NOMBRE SELECCIONADO PARA SER ELIMINADO
        db.delete(ShoppingElementEntry.TABLE_NAME, selection, selectionArgs);

    }
}
